

import { GestureHandlerRootView } from "react-native-gesture-handler";
import React from "react";
import { StatusBar, StyleSheet, View } from "react-native";
import { enableScreens } from "react-native-screens";
import { AppNavigator } from "./src/navigation/Navigator";
import { ColorSet } from "./src/styles";

enableScreens();
// Entery point of the app

const App = () => {
  return (
    <GestureHandlerRootView style={styles.container}>
      <StatusBar
        translucent

        barStyle={'dark-content'}
        backgroundColor={ColorSet.white}
      />
      <View style={styles.container}>
        <AppNavigator />
      </View>
    </GestureHandlerRootView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: ColorSet.white
  },
});
export default App;
