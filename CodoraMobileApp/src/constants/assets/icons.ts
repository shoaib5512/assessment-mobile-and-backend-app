export enum Icons {
    //social
    facebook = require('../../assets/images/icons/facebook.png'),
    google = require("../../assets/images/icons/google.png"),
    apple = require('../../assets/images/icons/apple.png'),

    //bottom tab
    home = require('../../assets/images/tabbar/home.png'),
    eye = require('../../assets/images/tabbar/eye.png'),
    bars = require('../../assets/images/tabbar/bars.png'),

    // other
    flag = require('../../assets/images/icons/flag.png'),
    analytics = require('../../assets/images/icons/analytics.png'),
    // heart_filled = require('../../assets/images/icons/heart_filled.png'),
    heart = require('../../assets/images/icons/heart.png'),
    location = require('../../assets/images/icons/location.png'),
    search = require('../../assets/images/icons/search.png'),
    back_button = require('../../assets/images/icons/back.png'),


}
