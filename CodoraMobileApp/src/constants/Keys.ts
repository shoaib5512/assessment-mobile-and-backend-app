export const Keys = {
  USER: "user",
  TOKEN: "token",
  FCM_TOKEN: "fcmToken",
  PERMISSION: "permissions",
  USER_TYPE: "userType",
};
