import { Images } from "./assets/images";
//this is Dummy / test data
const AppData = {
    //Dummy chart data
    Chart: [
        { name: 'PA', value: 570, color: '#005d60' },
        { name: '1B', value: 123, color: '#ff4b4a' },
        { name: '2B', value: 200, color: '#e849bb' },
        { name: '3B', value: 400, color: '#7f5cdd' },
        { name: '2C', value: 20, color: '#00a7ad' },
        { name: '3C', value: 310, color: '#81cc51' },
        { name: '1D', value: 60, color: '#ffac69' }
    ],
};

export default AppData;
