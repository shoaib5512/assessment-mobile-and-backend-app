export enum Screen {
  SplashStack = "SplashStack",
  SplashScreen = "SplashScreen",
  AuthStack = "AuthStack",
  SignupScreen = "SignupScreen",
  RoleScreen = "RoleScreen",

  //tab screen
  HomeScreen = "HomeScreen",
  ExploreScreen = "ExploreScreen",
  BottomTab = 'BottomTab',
}
