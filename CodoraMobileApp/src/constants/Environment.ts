export const domain = `https://test-domain.com`;
export const baseURL = `${domain}/api/`;
export const mode = "debug"; //release , debug

export enum Route {
  // Auth APIs endpoint
  SIGNUP = "/auth/signup",
}
