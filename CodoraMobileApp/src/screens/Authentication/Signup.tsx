import React, { useState } from "react";
import { StyleSheet, View, ScrollView, Alert, Linking } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Button, H1, InputComponent, Loader, Paragraph, Spacer, Toast } from "../../components";
import appStyle from "../../styles/appStyle";
import { ColorSet, FamilySet } from "../../styles";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Icons } from "../../constants/assets/icons";
import { IconButton } from "react-native-paper";
import { validateSignup } from "../../utils/Validation";
import { signup } from "../../networking/AuthApiService";
import { screenHeight } from "../../styles/screenSize";
import Header from "../../components/header/Header";

const SignupScreen: React.FC<{ navigation: any; route: any }> = ({ navigation
}) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setconfirmPassword] = useState("");
  const [formError, setFormError] = useState<any>({});
  const [paswordSecure, setPasswordSecure] = useState<boolean>(false);
  const [confirmPaswordSecure, setConfirmPasswordSecure] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);

  const onSubmit = async () => {
    let params = {
      name,
      email,
      password,
      confirmPassword,
    };

    let errors = await validateSignup(params);
    setFormError(errors);
    if (Object.keys(errors).length === 0) {
      setLoading(true);
      setTimeout(() => {
      setLoading(false);
        Alert.alert(`Account created successfully!`);
      }, 2000)
      return;
      //Enable baseurl and end point from Environment file to call API
      let response = await signup(params);
      if (response?.success) {
        Alert.alert(`Account created successfully!`);
        // navigation to next screen
        return;
      }
      if (response?.error) {
        Toast(response?.error?.message);
      }
    }
  };

  return (
    <KeyboardAwareScrollView contentContainerStyle={styles.scroll}>
      <Header
        backNavigation={true}
        navigation={navigation}
      />
      <ScrollView showsVerticalScrollIndicator={false}>

        <View style={styles.main}>
          {/* <View /> */}
          <View style={appStyle.inputContainer}>
            <Spacer size="lg" />
            <View style={styles.buttonContainer}>
              <H1 children={'Sign Up '} />
            <Spacer size="lg" />
            <Spacer size="lg" />

            </View>
            <InputComponent
              placeholder={'Email address'}
              onChangeText={setEmail}
              value={email}
              errorText={formError?.email}
            />
            <InputComponent
              placeholder={"Username"}
              onChangeText={setName}
              value={name}
              errorText={formError?.name}
            />
            <InputComponent
              placeholder={"Password"}
              onChangeText={setPassword}
              value={password}
              rightIcon={Icons.eye}
              onPressRightIcon={() => setPasswordSecure(!paswordSecure)}
              secureTextEntry={paswordSecure}
              errorText={formError?.password}
            />
            <InputComponent
              placeholder={"Repeat password"}
              onChangeText={setconfirmPassword}
              value={confirmPassword}
              rightIcon={Icons.eye}
              onPressRightIcon={() => setConfirmPasswordSecure(!confirmPaswordSecure)}
              secureTextEntry={confirmPaswordSecure}
              errorText={formError?.confirmPassword}
            />
            <Spacer size="lg" />

            <View style={styles.btn}>
              <Button
                children={'Sign Up'}
                onPress={() => onSubmit()}
              />
            </View>
          </View>


          <View>
            <Spacer size="lg" />
            <Spacer size="lg" />
            <View style={styles.orContainer}>
              <Paragraph style={{ color: ColorSet.black }}>
                Login with
              </Paragraph>
            </View>
            <View style={appStyle.rowCenter}>
              <IconButton
                icon={Icons.google}
                style={styles.social}
                size={24}
                iconColor={null}
                onPress={() => console.log("===")}
              />
              <IconButton
                icon={Icons.facebook}
                style={styles.social}
                size={24}
                iconColor={null}
                onPress={() => console.log("===")}
              />
              <IconButton
                icon={Icons.apple}
                style={styles.social}
                size={24}
                iconColor={null}
                onPress={() => console.log("===")}
              />
            </View>
            <Spacer />
            <View style={styles.signup}>
              <TouchableOpacity
                onPress={() => Linking.openURL('https://www.google.com/')}
              >
                <Paragraph style={styles.orText}>
                  Terms of service
                </Paragraph>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <Loader isLoading={loading} />
      </ScrollView>
    </KeyboardAwareScrollView>
  );
};
export default SignupScreen;

const styles = StyleSheet.create({
  scroll: {
    flexGrow: 1,
    backgroundColor: ColorSet.greyLighter,

  },
  container: {
    flex: 1,
  },
  main: {
    flex: 1,
    ...appStyle.jcCenter,
    paddingHorizontal: 20,
  },
  btn: {
    ...appStyle.w80,
    ...appStyle.asCenter,
    ...appStyle.aiCenter,
  },
  signup: {
    ...appStyle.row,
    ...appStyle.asCenter,
    ...appStyle.pb5,
  },
  buttonContainer: {
    ...appStyle.jcCenter,
    ...appStyle.aiCenter,
    ...appStyle.m10,
  },
  iconStyle: {
    width: 26,
    height: 26,
  },
  orText: {
    color: ColorSet.black,
    fontSize: 16,
    fontFamily: FamilySet.latoBold,
    marginHorizontal: 10,
  },
  orContainer: {
    alignItems: "center",
    marginBottom: 20,
    marginTop: 10,
  },
  social: {
    backgroundColor: ColorSet.white,
    borderWidth: 1,
    borderColor: ColorSet.greyLight,
    borderRadius: 50,
    width: 44,
    height: 44,
    marginHorizontal: 8,
  },
});
