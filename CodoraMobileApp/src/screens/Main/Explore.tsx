import React, { useState } from "react";
import { Image, ScrollView, StyleSheet, View, Text } from "react-native";
import appStyle from "../../styles/appStyle";
import { BaseballBannerComponent, CircularChart, SearchIcon, Spacer } from "../../components";
import { ColorSet, FamilySet, Fonts } from "../../styles";
import { Icons } from "../../constants/assets/icons";
import { Images } from "../../constants/assets/images";
import { screenHeight } from "../../styles/screenSize";
import Header from "../../components/header/Header";
import AppData from "../../constants/AppData";

const ExploreScreen: React.FC<{ navigation: any; route: any }> = ({ navigation }) => {


    //onPress search icon
    const onPressSearch = () => {
        console.log('PRESSED ON SEARCH');
    }

    return (
        <View style={styles.container}>
            <Header
                title="EXPLORE"
                backNavigation={true}
                navigation={navigation}
                rightComponent={
                    <SearchIcon onPress={onPressSearch} />
                }
            />
        </View >
    );
};

export default ExploreScreen;

const styles = StyleSheet.create({
    container: {
        ...appStyle.flex1,
        backgroundColor: ColorSet.white,
    },
    main: {
        height: screenHeight.height100,
    },
});
