import React, { useState } from "react";
import { Image, ScrollView, StyleSheet, View, Text } from "react-native";
import appStyle from "../../styles/appStyle";
import { BaseballBannerComponent, CircularChart, SearchIcon, Spacer } from "../../components";
import { ColorSet, FamilySet, Fonts } from "../../styles";
import { Icons } from "../../constants/assets/icons";
import { Images } from "../../constants/assets/images";
import { screenHeight } from "../../styles/screenSize";
import Header from "../../components/header/Header";
import AppData from "../../constants/AppData";

const HomeScreen: React.FC<{ navigation: any; route: any }> = ({ navigation }) => {

    const [liked, setLiked] = useState<boolean>(false);

    //onPress search icon
    const onPressSearch = () => {
        console.log('PRESSED ON SEARCH');
    }

    // Render each item in the FlatList
    const renderItem = (item: any, index: number) => (
        <View key={index}>
            <View style={[appStyle.rowBtw, appStyle.pt10, appStyle.pb5]}>
                <View style={[appStyle.row]}>
                    <View style={[styles.circle, { borderColor: item?.color }]} />
                    <Text style={styles.name}>{item?.name}</Text>
                </View>
                <Text style={styles.value}>{item?.value}</Text>
            </View>
            <View style={appStyle.divider} />
        </View>
    );

    return (
        <View style={styles.container}>
            <Header
                title="BASEBALL NUMBERS"
                backNavigation={true}
                navigation={navigation}
                rightComponent={
                    <SearchIcon onPress={onPressSearch} />
                }
            />
            <ScrollView contentContainerStyle={appStyle.scroll}>
                <View style={styles.main}>
                    <BaseballBannerComponent
                        liked={liked}
                        image={Images.baseball}
                        name={'Hank Aron'}
                        skill={'Right Fielder'}
                        quality={'Milwaukee Braves'}
                        age={'February 15, 1934 (age 84)'}
                        location={'Mobile Alabama'}
                        height={`6'0"`}
                        weight={'180 lbs'}
                        batted={'Right'}
                        threw={'Left'}
                    />
                    <View style={appStyle.ph20}>
                        <View style={[appStyle.row, appStyle.pv10]}>
                            <Image style={styles.analytics} source={Icons.analytics} />
                            <Text style={styles.title}>Key Stats</Text>
                        </View>
                        <View style={appStyle.divider} />
                        <Spacer size="lg" />
                        <CircularChart data={AppData.Chart} />
                        <Spacer size="lg" />
                        <View style={appStyle.divider} />
                        {AppData?.Chart?.map((item, index) => (
                            renderItem(item, index)
                        ))}
                    </View>
                </View>
                <View style={appStyle.safeContainerBottom} />

                {/* </View> */}
            </ScrollView>
        </View >
    );
};

export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        ...appStyle.flex1,
        backgroundColor: ColorSet.white,
    },
    main: {
        height: screenHeight.height100,
    },
    analyticsRow: {
        ...appStyle.row,
    },
    analytics: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        marginRight: 10,
    },
    circle: {
        borderWidth: 3,
        height: 20,
        width: 20,
        marginRight: 15,
        marginTop: -4,
        borderRadius: 20,
    },
    name: {
        color: ColorSet.greyDarker,
        ...Fonts.size.small,
        fontFamily: FamilySet.poppinsMedium,
    },
    value: {
        color: ColorSet.greyDarker,
        ...Fonts.size.normal,
        fontFamily: FamilySet.poppinsMedium,
    },
    title: {
        color: ColorSet.greyDark,
        ...Fonts.size.small,
        fontFamily: FamilySet.poppins,
        textTransform: 'uppercase',
        paddingTop :4,
    },
});
