import React from "react";
import { StyleSheet, View } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Button, H2, H3, Logo, Spacer } from "../../components";
import appStyle from "../../styles/appStyle";
import { Screen } from "../../constants/screens/screens";
import { ColorSet } from "../../styles";

const SplashScreen: React.FC<{ navigation: any }> = ({ navigation }) => {

  return (
    <KeyboardAwareScrollView contentContainerStyle={appStyle.scrollContainer}>
      <View style={styles.main}>
        <View style={appStyle.aiCenter}>
          <Logo />
          <Spacer size="lg" />
          <H2 style={{ color: ColorSet.theme }}>CODORA</H2>
          <H3 style={{ color: ColorSet.theme }}>Assessment App</H3>
        </View>
        <Spacer size="lg" />
        <Spacer size="lg" />

        <View>
          <Button
            children={'Sign Up Form'}
            onPress={() => navigation.navigate(Screen.AuthStack)}
          />
        </View>
        <Spacer size="lg" />

        <View>
          <Button
            children={'Dashboard Screen'}
            onPress={() => navigation.navigate(Screen.BottomTab)}
          />
        </View>

      </View>
    </KeyboardAwareScrollView>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  main: {
    ...appStyle.flex1,
    ...appStyle.jcCenter,
    ...appStyle.aiCenter,
    backgroundColor: ColorSet.white
  },
});
