import React from "react";
import {
    StyleSheet,
    TouchableOpacity,
    Image,
    ViewStyle,
    ImageSourcePropType,
    Text,
    View,
    ImageBackground,
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { ColorSet, Fonts, FamilySet } from "../styles";
import appStyle from "../styles/appStyle";
import { Icons } from "../constants/assets/icons";
import { screenWidth } from "../styles/screenSize";
import { Spacer } from ".";

interface Props {
    containerStyle?: ViewStyle | undefined;
    onPress?: (() => void) | undefined;
    liked?: boolean;
    image?: ImageSourcePropType | undefined;
    name: String | undefined;
    skill: String | undefined;
    quality: String | undefined;
    age: String | undefined;
    location: String | undefined;
    height: String | undefined;
    weight: String | undefined;
    batted: String | undefined;
    threw: String;
}

const BaseballBannerComponent: React.FC<Props> = (props) => {
    const {
        containerStyle,
        onPress,
        liked,
        image,
        name,
        skill,
        quality,
        age,
        location,
        height,
        weight,
        batted,
        threw
    } = props;

    return (
        <ImageBackground
            source={image}
            style={[{ ...styles.container, ...containerStyle }]}
            imageStyle={{
                resizeMode: "cover",
                width: screenWidth.width100,
                height: screenWidth.width100,
            }}>
            <LinearGradient
                colors={['transparent', 'rgba(0,0,0,1)']}
                style={styles.container}
            />
            <TouchableOpacity style={styles.wishView}>
                <Image style={styles.heart} source={Icons.heart} />
            </TouchableOpacity>
            <View style={styles.wrapper}>
                <View style={[appStyle.row, appStyle.aiCenter]}>
                    <Text style={styles.title}>{name}</Text>
                    <Image style={styles.flag} source={Icons.flag} />
                </View>
                <Spacer size="xs" />
                <Text style={styles.description}>{skill}</Text>
                <Text style={styles.description}>{quality}</Text>

                <Text style={styles.description}>{age}</Text>
                <View style={[appStyle.row, appStyle.aiCenter]}>
                    <Text style={styles.highlightedDescription}>{location}</Text>
                    <Image style={styles.location} source={Icons.location} />
                </View>
                <Spacer size="md" />
                <View style={styles.skillRow}>
                    <View style={[styles.skillColumn, styles.noBorder]}>
                        <Text style={styles.skillTitle}>Height</Text>
                        <Text style={styles.skillValue}>{height}</Text>
                    </View>
                    <View style={styles.skillColumn}>
                        <Text style={styles.skillTitle}>Weight</Text>
                        <Text style={styles.skillValue}>{weight}</Text>
                    </View>
                    <View style={styles.skillColumn}>
                        <Text style={styles.skillTitle}>Batted</Text>
                        <Text style={styles.skillValue}>{batted}</Text>
                    </View>
                    <View style={styles.skillColumn}>
                        <Text style={styles.skillTitle}>Threw</Text>
                        <Text style={styles.skillValue}>{threw}</Text>
                    </View>
                </View>
            </View>
        </ImageBackground >
    );
};

const styles = StyleSheet.create({
    container: {
        width: screenWidth.width100,
        height: screenWidth.width100,
        // marginHorizontal: -10,
    },
    wrapper: {
        position: 'absolute',
        bottom: 0, // Align to the bottom
        left: 0, // Align to the left
        padding: 15,
    },
    wishView: {
        position: 'absolute',
        top: 10, // Align to the top
        right: 10, // Align to the right
        padding: 10,
    },
    skillRow: {
        ...appStyle.rowBtw,
        width: '95%',
    },
    skillColumn: {
        paddingLeft: 10,
        borderLeftColor: ColorSet.greyDark,
        borderLeftWidth: 1,
    },
    noBorder: {
        borderLeftColor: ColorSet.transparent,
    },
    heart: {
        tintColor: ColorSet.white,
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    title: {
        color: ColorSet.white,
        ...Fonts.size.large,
        fontFamily: FamilySet.poppinsBold,
        textTransform: 'uppercase',
        lineHeight:40,
    },
    flag: {
        width: 30,
        height: 30,
        resizeMode: 'contain',
        marginLeft: 10,
    },
    description: {
        color: ColorSet.white,
        ...Fonts.size.normal,
        fontFamily: FamilySet.latoRegular,
        marginBottom: 1,
    },
    highlightedDescription: {
        color: ColorSet.red,
        ...Fonts.size.normal,
        marginBottom: 1,
    },
    location: {
        width: 15,
        height: 20,
        resizeMode: 'contain',
        marginLeft: 5,
    },
    skillTitle: {
        color: ColorSet.grey,
        ...Fonts.size.small,
        fontFamily: FamilySet.latoRegular,
        marginBottom: 2,
    },
    skillValue: {
        color: ColorSet.white,
        ...Fonts.size.normal,
        fontFamily: FamilySet.poppinsMedium,
    }
});

export default BaseballBannerComponent;