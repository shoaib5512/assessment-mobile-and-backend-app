import React from 'react';
import { StyleSheet, View } from 'react-native';

interface SpacerProps {
  size?: string;
}

const Spacer: React.FC<SpacerProps> = props => {
  const { size } = props;

  return (
    <View>
      {size === 'xs' ? <View style={styles.xs} /> : null}
      {size === 'sm' ? <View style={styles.sm} /> : null}
      {!size || size === 'md' ? <View style={styles.md} /> : null}
      {size === 'lg' ? <View style={styles.lg} /> : null}
      {size === 'xl' ? <View style={styles.xl} /> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  xs: {
    width: '100%',
    height: 4,
  },
  sm: {
    width: '100%',
    height: 8,
  },
  md: {
    width: '100%',
    height: 16,
  },
  lg: {
    width: '100%',
    height: 24,
  },
  xl: {
    width: '100%',
    height: 32,
  },
});

export default Spacer;
