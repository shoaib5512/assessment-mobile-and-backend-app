import React from "react";
import {StyleSheet, TextStyle, View} from "react-native";
import {ColorSet} from "../../styles";
import appStyle from "../../styles/appStyle";

interface MainLayoutProps {
  style?: TextStyle | undefined;
  children?: React.ReactNode;
  navigation?: any | undefined;
}

const MainLayout: React.FC<MainLayoutProps> = (props) => {
  const {style, children, navigation} = props;
  return (
    <View style={[styles.wrapper, {...style}]}>
      <View style={styles.container}>{children}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    ...appStyle.flex1,
    backgroundColor: ColorSet.white,
  },
  container: {
    ...appStyle.flex1,
  },
});

export default MainLayout;
