import React, { useState } from 'react';
import { StyleSheet, Image, View, Text, TextInput as RNTextdInput, ImageSourcePropType, KeyboardTypeOptions, ViewStyle, TextStyle, TouchableOpacity } from 'react-native';
import { useTheme } from "@react-navigation/native";
import { ColorSet, FamilySet, Fonts } from '../styles';
import appStyle from '../styles/appStyle';
import { TextInput as RNTextInput } from "react-native-paper";


interface InputComponentProps {
    label?: string;
    value: string;
    onChangeText: (text: any) => void;
    placeholder?: string;
    leftIcon?: ImageSourcePropType;
    style?: any; // You can define a more specific type for style if needed
    containerStyle?: ViewStyle; // You can define a more specific type for containerStyle if needed
    secureTextEntry?: boolean;
    keyboardType?: KeyboardTypeOptions | undefined;
    textStyle?: TextStyle; // You can define a more specific type for textStyle if needed
    leftBottomText?: string;
    maxLength?: number;
    multiline?: boolean;
    rightIcon?: ImageSourcePropType | undefined;
    isDropdown?: boolean;
    onPressRightIcon?: (() => void) | any;
    editable?: boolean
    errorText?: string;
}

const InputComponent: React.FC<InputComponentProps> = ({
    label,
    value,
    onChangeText,
    placeholder,
    leftIcon,
    style,
    containerStyle,
    secureTextEntry,
    keyboardType,
    textStyle,
    leftBottomText,
    maxLength,
    multiline,
    rightIcon,
    onPressRightIcon,
    editable = true,
    errorText,
}) => {
    const { colors } = useTheme();
    const [characterCount, setCharacterCount] = useState(value ? value.length : 0);


    const error = maxLength && characterCount === maxLength ? "Limit reached" : null;

    const onTextChange = (text: string) => {
        if (maxLength && text.length > maxLength) {
            // Notify when character limit is reached
            return;
        }
        // Update character count and call the parent component's onChangeText function
        setCharacterCount(text.length);
        onChangeText(text);
    };

    return (
        <View style={[appStyle.mb10, styles.container]}>
            {label && (
                <View style={appStyle.pt5}>
                    <Text style={{ ...styles.label, ...textStyle }}>{label}</Text>
                </View>
            )}
            <View style={[styles.input, multiline && styles.multilineInput, containerStyle]}>
                <RNTextInput
                    placeholder={placeholder}
                    onChangeText={onTextChange}
                    value={value}
                    secureTextEntry={secureTextEntry}
                    keyboardType={keyboardType}
                    placeholderTextColor={ColorSet.grey}
                    style={[styles.rectangle, multiline && styles.multilineText]} // Adjust style for multiline
                    multiline={multiline} // Set the multiline property of the TextInput
                    editable={editable}
                />
                {!!rightIcon && (
                    <TouchableOpacity
                        onPress={onPressRightIcon}
                        style={styles.rightIconView}
                    >
                        <Image style={styles.rightIcon} source={rightIcon} />
                    </TouchableOpacity>
                )}
            </View>
            <View style={styles.bottomTextContainer}>
                {(error || errorText) && (
                    <Text style={styles.bottomLeftTextError}>{error || errorText}</Text>
                )}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
    },
    input: {
        ...appStyle.row,
        ...appStyle.aiCenter,
        ...appStyle.asCenter,
        ...appStyle.jcCenter,
        ...appStyle.mb5,
        ...appStyle.mt5,
        minWidth: '100%',
    },
    leftIcon: {
        width: 20,
        height: 20,
        marginLeft: 40,
    },
    rectangle: {
        borderColor: ColorSet.greyDark,
        backgroundColor: ColorSet.greyLighter,
        paddingHorizontal: 0,
        ...appStyle.flex1,
        textAlignVertical: "top",
        color: ColorSet.black,
        ...Fonts.size.small,
        fontFamily: FamilySet.regular,
        minWidth: '100%',
        height: 40,
    },
    textContainer: {
        alignItems: 'flex-start',
    },
    label: {
        color: ColorSet.black,
        ...Fonts.size.xsmall,
        fontFamily: FamilySet.poppins,
    },
    bottomTextContainer: {
        ...appStyle.row,
    },
    bottomLeftText: {
        color: ColorSet.textTitle,
        ...Fonts.size.small,
        fontFamily: FamilySet.poppins,
    },
    bottomLeftTextError: {
        color: ColorSet.errorMain,
        ...Fonts.size.small,
        fontFamily: FamilySet.poppins,
    },
    bottomRightText: {
        color: ColorSet.textSubtitle,
        ...Fonts.size.small,
        fontFamily: FamilySet.poppins,
    },
    multilineInput: {
        height: 250, // Adjust the height for multiline
    },
    multilineText: {
        height: '100%', // Allow the TextInput to expand to its parent's height
    },
    rightIconView: {
        position: "absolute",
        right: -10,
        top: 0,
        height: 40,
        width: 50,
        justifyContent: "center",
        alignItems: "center",
    },
    rightIcon: {
        width: 20,
        height: 20,
        resizeMode: "contain",
        tintColor: ColorSet.greyDark,
    },
});

export default InputComponent;
