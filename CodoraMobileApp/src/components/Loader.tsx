import React from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import { Chase } from 'react-native-animated-spinkit';
import { ColorSet } from '../styles';
import { H5 } from '.';
import appStyle from '../styles/appStyle';

const layoutStyle = {
  inside: 'inside',
  outside: 'outside',
};

interface LoaderProps {
  isLoading?: boolean | undefined;
  overlay?: boolean | undefined;
  shadow?: boolean | undefined;
  message?: String | undefined;
  layout?: 'inside' | 'outside' | undefined;
}

const Loader: React.FC<LoaderProps> = props => {
  const { isLoading, overlay, shadow, message, layout } = props;

  return (
    <>
      {isLoading && (
        <View
          style={[
            styles.container,
            {
              backgroundColor: ColorSet.transparent,
            },
          ]}
        >
          <View style={[styles.loaderContainer, shadow == false ? null : styles.loaderContainerShadow]}>
            {/* <Chase style={styles.loader} size={48} color={ColorSet.blueDark} animating={isLoading}></Chase> */}
            <ActivityIndicator size={'large'} />
            {layout == layoutStyle?.inside ||
              (layout == undefined && !!message && (
                <View style={appStyle.pt5}>
                  <H5>{message}</H5>
                </View>
              ))}
          </View>
          {layout == layoutStyle?.outside && !!message && (
            <View style={appStyle.pt5}>
              <H5>{message}</H5>
            </View>
          )}
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    top: 0,
    height: '110%',
    width: '100%',
    shadowColor: 'rgba(0,0,0,0.16)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.16,
    shadowRadius: 3.0,

    elevation: 23,
  },
  loaderContainer: {
    alignItems: 'center',
    padding: 13,
  },
  loaderContainerShadow: {
    backgroundColor: ColorSet.transparent,
    // shadowColor: ColorSet.black,
    // shadowOffset: {
    //   width: 0,
    //   height: 3,
    // },
    // shadowOpacity: 0.12,
    // shadowRadius: 3.97,

    // elevation: 17,
    // borderRadius: 8,
    margin: 0,
  },
  loader: {},
  logo: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
});

export default Loader;
