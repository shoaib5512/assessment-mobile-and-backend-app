import React from "react";
import {
    StyleSheet,
    TouchableOpacity,
    Image,
    ImageStyle,
    ViewStyle,
    ImageSourcePropType,
} from "react-native";

import { ColorSet } from "../../styles";
import { Icons } from "../../constants/assets/icons";

interface Props {
    containerStyle?: ViewStyle | undefined;
    iconStyle?: ImageStyle | undefined;
    icon?: ImageSourcePropType | undefined;
    onPress?: (() => void) | undefined;
}

const SearchIcon: React.FC<Props> = (props) => {
    const {
        containerStyle,
        onPress,
        icon,
        iconStyle,
    } = props;

    return (
        <TouchableOpacity
            onPress={onPress}
            style={[{ ...styles.container, ...containerStyle }]}
        >
            <Image
                source={icon ? icon : Icons.search}
                style={[{ ...styles.icon, ...iconStyle, }]}

            />
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    icon: {
        width: 30,
        height: 30,
        resizeMode: "contain",
        tintColor: ColorSet.theme,
    },
});

export default SearchIcon;