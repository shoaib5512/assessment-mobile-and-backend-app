import React from "react";
import { View, Image, StyleSheet, TouchableOpacity } from "react-native";
import { Icons } from "../../constants/assets/icons";
import { ColorSet } from "../../styles";
import appStyle from "../../styles/appStyle";

interface BackButtonProps {
  onPress?: (() => void) | undefined;
  tint?: string
}

const BackButton: React.FC<BackButtonProps> = (props) => {
  const { onPress, tint } = props;
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPress} style={styles.backButton}>
        <View>
          <Image source={Icons.back_button} style={[styles.icon, {
            tintColor: tint ? tint : ColorSet.theme
          }]} />
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backButton: {
    padding:2
  },
  icon: {
    width: 22,
    height: 22,
    resizeMode: 'contain',
  },
});

export default BackButton;
