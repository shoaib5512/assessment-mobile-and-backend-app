import React, { ReactNode } from "react";
import {
  View,
  StyleSheet,
  TextStyle,
  Dimensions,
  TouchableOpacity,
  Platform,
} from "react-native";
import { useNavigation } from "@react-navigation/core";
import BackButton from "./BackButton";
import H2 from "../typography/H2";
import { ColorSet, FamilySet, Fonts } from "../../styles";
import { screenWidth } from "../../styles/screenSize";

const { width } = Dimensions.get("window");
const maxWidth = width;

interface HeaderProps {
  navigation?: any | undefined;
  backNavigation?: boolean;
  rightComponent?: React.ReactNode;
  style?: TextStyle | undefined;
  titleStyle?: TextStyle | undefined;
  title?: string;
  theme?: "dark" | "light";
  onPressRightComponent?: (() => void) | undefined;
  children?: ReactNode;
}

const Header: React.FC<HeaderProps> = (props) => {
  const { goBack } = useNavigation();
  const {
    rightComponent,
    title,
    backNavigation = true,
    titleStyle,
    navigation,
    children,
    onPressRightComponent,
  } = props;

  return (
    <View>
      <View style={styles.container}>
        <View>
          {backNavigation && (
            <View style={styles.backButton}>
              <BackButton onPress={() => goBack()} />
            </View>
          )}
        </View>
        <View style={styles.heading}>
          <H2 style={{ ...styles.head, ...titleStyle }}>{title}</H2>
        </View>
        <View>
          {rightComponent && <TouchableOpacity onPress={() => onPressRightComponent} style={styles.right}>{rightComponent}</TouchableOpacity>}
        </View>
      </View>
      {children && children}
    </View >
  );
};

const styles = StyleSheet.create({
  container: {
    width: maxWidth,
    height:  Platform.OS == 'ios' ? 90 :75,
    paddingTop: Platform.OS == 'ios' ? 55 : 40,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    backgroundColor: ColorSet.transparent
  },
  backButton: {
    position: "absolute",
  },
  heading: {
    width: screenWidth.width100 - 125,
    alignItems: "center",
  },
  right: {
    position: "absolute",
    right: 5,
  },
  head: {
    color: ColorSet.theme,
    fontFamily: FamilySet.poppinsBold,
    fontSize: 20,
  },
});

export default Header;
