import React from "react";
import {
    StyleSheet,
    View,
    Text,
} from "react-native";
import { DonutChart } from "react-native-circular-chart";
import { ColorSet } from "../styles";
import { screenWidth } from "../styles/screenSize";
import { Fonts, FamilySet } from "../styles";

interface Props {
    data: Array<any>
}

const CircularChart: React.FC<Props> = (props) => {
    const {
        data
    } = props;

    return (
        <View style={styles.chartWrapper}>
            <DonutChart
                data={data}
                strokeWidth={30}
                radius={90}
                // containerWidth={screenWidth.width50 + 5}
                // containerHeight={screenWidth.width50 + 5}
                containerWidth={245}
                containerHeight={245}
                type="butt"
                startAngle={0}
                endAngle={360}
                animationType="slide"
                labelValueStyle={{
                    color: ColorSet.black
                }}
                labelTitleStyle={{
                    color: ColorSet.black
                }}
            />
            <View style={styles.chartInner}>
                <Text style={styles.total}>13941</Text>
                <Text style={styles.label}>PA</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    chartWrapper: {
        width: 220,
        height: 220,
        // width: screenWidth.width50 + 5,
        // height: screenWidth.width50 + 5,
        borderRadius: 200,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: 'center',
        backgroundColor: ColorSet.white,
        shadowColor: ColorSet.greyDark,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.1,
        shadowRadius: 5.41,
        elevation: 3,
    },
    chartInner: {
        width: 138,
        height: 138,
        // width: screenWidth.width50 - 80,
        // height: screenWidth.width50 - 80,
        borderWidth: 5,
        borderColor: ColorSet.white,
        borderRadius: 200,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: ColorSet.white,
        shadowColor: ColorSet.greyDark,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.1,
        shadowRadius: 4.41,
        elevation: 3,
        position: 'absolute',
    },
    total: {
        color: ColorSet.black,
        ...Fonts.size.xmedium,
        fontFamily: FamilySet.poppinsBold,
    },
    label: {
        color: ColorSet.greyDarker,
        ...Fonts.size.normal,
        fontFamily: FamilySet.poppinsMedium,
    }
});

export default CircularChart;