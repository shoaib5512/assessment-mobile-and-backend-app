import SimpleToast from 'react-native-simple-toast';

export function Toast(message: string) {
    SimpleToast.show(message);
}