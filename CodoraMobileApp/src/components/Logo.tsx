import React from 'react';
import { Image, ImageStyle, StyleSheet, View, ViewStyle } from 'react-native';

import appStyle from '../styles/appStyle';
import { screenWidth } from '../styles/screenSize';
import { Images } from '../constants/assets/images';
import { ColorSet } from '../styles';

interface LogoProps {
  style?: ViewStyle | undefined;
  imageStyle?: ImageStyle | undefined;
  image?: any | undefined
}

const Logo: React.FC<LogoProps> = props => {
  const { style, imageStyle, image } = props;
  return (
    <View style={[appStyle.aiCenter, { ...style }]}>
      <Image
        style={{
          ...styles.image,
          ...imageStyle,
        }}
        source={image ?? Images.logo}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    width: screenWidth.width40,
    height: 120,
    resizeMode: 'contain',
  },
});

export default Logo;
