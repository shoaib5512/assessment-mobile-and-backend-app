import React from 'react'
import { StyleSheet, Text, ViewStyle, View } from 'react-native'
import { ColorSet, Fonts } from '../styles'

interface ErrorTextProps {
    text: string,
    containerStyle?: ViewStyle
}

const ErrorText: React.FC<ErrorTextProps> = ({ text, containerStyle }: ErrorTextProps) => {
    return (
        <View style={containerStyle}>
            <Text style={styles.text}>{text}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    text: {
        fontWeight: 'normal',
        color: ColorSet.red,
        ...Fonts.size.medium,
        textAlign: 'center'
    }
})

export default ErrorText