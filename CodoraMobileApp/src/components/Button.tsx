import React from "react";
import {
  StyleSheet,
  Text,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
  View,
} from "react-native";

import { ColorSet, Fonts, FamilySet } from "../styles";

interface ButtonProps {
  style?: TextStyle | undefined;
  containerStyle?: ViewStyle | undefined;
  onPress?: (() => void) | undefined;
  disable?: boolean;
  children?: React.ReactNode;
  btnFixed?: boolean;
}

const Button: React.FC<ButtonProps> = (props) => {
  const {
    style,
    disable,
    containerStyle,
    onPress,
    children,
    btnFixed,
  } = props;

  return (
    <TouchableOpacity
      disabled={disable}
      onPress={onPress}
      style={[
        { ...styles.container, ...containerStyle },
      ]}
    >
        <View style={[styles.inner]}>
          <Text
            style={[
              { ...styles.label, ...style },
              btnFixed && styles.btnFixedText,
            ]}
          >
            {children}
          </Text>
        </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 35,
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor :ColorSet.greyLighter,
    shadowColor: ColorSet.greyDark,
    shadowOffset: {
        width: 0,
        height: 1,
    },
    shadowOpacity: 0.5,
    shadowRadius: 5.41,
    elevation: 10,
  },
  inner: {
    height: 60,
    flex: 1,
    borderRadius: 50,
    paddingHorizontal :20,
    paddingTop:5,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",

  },
  label: {
    color: ColorSet.greyDark,
    ...Fonts.size.medium,
    fontFamily: FamilySet.poppins,
    textAlign: 'center'
  },
  btnFixedText: {
    ...Fonts.size.small,
  },
});

export default Button;
