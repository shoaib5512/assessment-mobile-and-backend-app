// Utility function to validate an email address
export const validateEmail = (email: string) => {
  //   const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
  return emailRegex.test(email);
};

// Utility function to validate a password
const validatePassword = (password: string) => {
  // Check if the password is at least 8 characters long
  if (password?.length < 8) {
    return false;
  }
  return true;
};

export const validateSignup = (param: any) => {
  let errors: any = {};
  if (param?.name == "") {
    errors.name = "Please enter your name.";
  }
  if (!validateEmail(param?.email)) {
    errors.email = "Please enter valid email address.";
  }
  if (!validatePassword(param?.password)) {
    errors.password = "Please enter atleast 8 digit password.";
  }
  if (!validatePassword(param?.confirmPassword)) {
    errors.confirmPassword = "Please enter atleast 8 digit Password.";
  }
  if (param?.confirmPassword !== param?.password) {
    errors.confirmPassword = "Password and confirm password did not match.";
  }
  return errors;
};
