import { Method, fetchData } from "./NetworkManager";
import { Route } from "../constants/Environment";

export const signup = (data: any) => {
  return fetchData(Route.SIGNUP, Method.POST, data, false, false, false)
    .then(async (result) => {
      return result;
    })
    .catch((error) => {
      return {
        success: false,
        message: "",
        data: {},
        error: {
          code: 500,
          message: error?.message || error,
        },
      };

    });
};