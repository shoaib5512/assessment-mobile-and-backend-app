import React from "react";
import { useColorScheme, ActivityIndicator, StatusBar } from "react-native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from "@react-navigation/native";
import { Screen } from "../constants/screens/screens";
import {
  SplashScreen,
} from "../screens";
import SignupScreen from "../screens/Authentication/Signup";
import { ColorSet } from "../styles";
import BottomTab from "./BottomTab";

const AppDefaultTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: ColorSet.black,
    text: ColorSet.grey,
  },
};

const Stack = createNativeStackNavigator();

const SplashStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name={Screen.SplashScreen} component={SplashScreen} />
    </Stack.Navigator>
  );
};

const AuthStack = () => {
  return (
    <Stack.Navigator
      initialRouteName={Screen.SignupScreen}
      screenOptions={{
        headerShown: false,
        animation: "flip",
        gestureEnabled: true,
        presentation: "card",
      }}
    >
      <Stack.Screen name={Screen.SignupScreen} component={SignupScreen} />
    </Stack.Navigator>
  );
};

function AppNavigator() {
  const scheme = useColorScheme();

  return (
    <NavigationContainer
      theme={scheme === "dark" ? DarkTheme : AppDefaultTheme}
      fallback={<ActivityIndicator color="red" size="large" />}
    >
      <StatusBar
        translucent
        barStyle={'dark-content'}
        backgroundColor={ColorSet.white}
      />
      <Stack.Navigator
        initialRouteName={Screen.SplashStack}
        screenOptions={{
          headerShown: false,
        }}
      >
        {/* // stack for Entery Screen */}
        <Stack.Screen name={Screen.SplashStack} component={SplashStack} />

        {/* Stack for Auth Screen */}
        <Stack.Screen name={Screen.AuthStack} component={AuthStack} />

        {/* Stack for Bottom tab screen */}
        <Stack.Screen name={Screen.BottomTab} component={BottomTab} />

      </Stack.Navigator>
    </NavigationContainer>
  );
}
export { AppNavigator };
