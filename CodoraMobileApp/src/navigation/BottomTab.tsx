import React from "react";
import { useColorScheme, Platform } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { ColorSet } from "../styles";
import { Screen } from "../constants/screens/screens";
import { Image } from "react-native";
import { Icons } from "../constants/assets/icons";
import appStyle from "../styles/appStyle";
import {
    ExploreScreen,
    HomeScreen
} from "../screens";

const Tab = createBottomTabNavigator();

function BottomTab() {
    const colorScheme = useColorScheme();
    const isDarkMode = colorScheme === 'dark';
    const isAndroid = Platform.OS === 'android'

    // TODO : 
    // Can use different color for active and non-active tab icons
    // i.e for tabBarActiveTintColor, tabBarInactiveTintColor, appStyle.tabIconFocused

    return (
        <Tab.Navigator
            initialRouteName={Screen.HomeScreen}
            screenOptions={{
                tabBarHideOnKeyboard: true,
                headerShown: false,
                tabBarActiveTintColor: isDarkMode ? ColorSet.theme : ColorSet.theme,
                tabBarInactiveTintColor: isDarkMode ? ColorSet.theme : ColorSet.theme,
                tabBarStyle: { height: Platform.OS == 'ios' ? 70 : 60, borderTopColor: ColorSet.grey, backgroundColor: ColorSet.white },
                tabBarLabelStyle: { paddingBottom: isAndroid ? 8 : 0, marginBottom: isAndroid ? 0 : -10, fontSize: 14 },
            }}
        >
            <Tab.Screen
                name={'Home'} component={HomeScreen} options={{
                    tabBarIcon: ({ focused }) => (
                        <Image source={Icons.home}
                            // TODO :  Can use different icons for active and non-active tab icons
                            style={focused ? (isDarkMode ? appStyle.tabIconFocused : appStyle.tabIconFocused)
                                : (isDarkMode ? appStyle.tabIconDark
                                    : appStyle.tabIcon)}
                        />
                    )
                }}
            />
            <Tab.Screen
                name={'Explore'} component={ExploreScreen} options={{
                    tabBarIcon: ({ focused }) => (
                        <Image source={focused ? Icons.eye : Icons.eye}
                            style={focused ? (isDarkMode ? appStyle.tabIconFocused : appStyle.tabIconFocused)
                                : (isDarkMode ? appStyle.tabIconDark
                                    : appStyle.tabIcon)}
                        />
                    )
                }}
            />
            <Tab.Screen
                name={'Numbers'} component={HomeScreen} options={{
                    tabBarIcon: ({ focused }) => (
                        <Image source={focused ? Icons.bars : Icons.bars}
                            style={focused ? (isDarkMode ? appStyle.tabIconFocused : appStyle.tabIconFocused)
                                : (isDarkMode ? appStyle.tabIconDark
                                    : appStyle.tabIcon)}
                        />
                    )
                }}
            />
        </Tab.Navigator>
    );
};

export default BottomTab;
