This is a new [**React Native**](https://reactnative.dev) project, bootstrapped using [`@react-native-community/cli`](https://github.com/react-native-community/cli).

# React Native App
 Technologies Used
- React Native CLI
- TypeScript

# TypeScript Advantage
-TypeScript offers several advantages over plain JavaScript:

- Static Typing: TypeScript allows you to define types for variables, which helps catch errors during development and improves code readability.
- Enhanced Tooling: TypeScript provides better IDE support with features like code completion, refactoring tools, and navigation to definitions.
- Improved Maintainability: With static typing and better tooling, TypeScript helps maintain large codebases more effectively by reducing bugs and making it easier to understand and refactor code.


# Getting Started

>**Note**: Make sure you have completed the [React Native - Environment Setup](https://reactnative.dev/docs/environment-setup) instructions till "Creating a new application" step, before proceeding.

## Step 1: Start the Metro Server

First, you will need to start **Metro**, the JavaScript _bundler_ that ships _with_ React Native.

# How to Run

- To run the React Native project, follow these steps:
- To start Metro, run the following command from the _root_ of your React Native project:

```bash
# using npm
npm start

# OR using Yarn
yarn start
```

## Step 2: Start your Application

- Install Dependencies: Navigate to the project directory and install dependencies using:
   npm install.
- for ios, move to ios directory, and un : pod install and then move back to root directory.
- Start Metro Server: Start the Metro bundler server using npx react-native start.
- Run on Android: To run the app on an Android emulator or device, use npx react-native run-android.
- Run on iOS: To run the app on an iOS simulator or device, use npx react-native run-ios.


## Step 3: Modifying your App (Not Recommended)

Now that you have successfully run the app, let's modify it.

1. Open `App.tsx` in your text editor of choice and edit some lines.
2. For **Android**: Press the <kbd>R</kbd> key twice or select **"Reload"** from the **Developer Menu** (<kbd>Ctrl</kbd> + <kbd>M</kbd> (on Window and Linux) or <kbd>Cmd ⌘</kbd> + <kbd>M</kbd> (on macOS)) to see your changes!

   For **iOS**: Hit <kbd>Cmd ⌘</kbd> + <kbd>R</kbd> in your iOS Simulator to reload the app and see your changes!

## Congratulations! :tada:

You've successfully run and modified your React Native App. :partying_face:

### Now what?

- If you want to add this new React Native code to an existing application, check out the [Integration guide](https://reactnative.dev/docs/integration-with-existing-apps).
- If you're curious to learn more about React Native, check out the [Introduction to React Native](https://reactnative.dev/docs/getting-started).

# Troubleshooting

If you can't get this to work, see the [Troubleshooting](https://reactnative.dev/docs/troubleshooting) page.

# Learn More

To learn more about React Native, take a look at the following resources:

- [React Native Website](https://reactnative.dev) - learn more about React Native.
- [Getting Started](https://reactnative.dev/docs/environment-setup) - an **overview** of React Native and how setup your environment.
- [Learn the Basics](https://reactnative.dev/docs/getting-started) - a **guided tour** of the React Native **basics**.
- [Blog](https://reactnative.dev/blog) - read the latest official React Native **Blog** posts.
- [`@facebook/react-native`](https://github.com/facebook/react-native) - the Open Source; GitHub **repository** for React Native.
