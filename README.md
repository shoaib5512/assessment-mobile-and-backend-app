# Assessment Mobile and Backend app



## Getting started

To make it easy for you to get started with this assessment, here's a list of recommended next steps.

##  For Mobile app, move to the CodoraMobileApp directory and follow the CodoraMobileApp/README.md instruction file to compile the project


##  For Backend app, move to the CodoraBackend directory and follow the CodoraBackend/README.md instruction file to compile the project