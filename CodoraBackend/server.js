const express = require('express');
const mongoose = require('mongoose');
const app = express();
const dbURI = 'mongodb://localhost/codora';

//define express
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

///DB connection 

mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('Connected to MongoDB'))
    .catch(error => console.error('MongoDB connection error:', error));

//Listen Port
app.listen(3000, () => {
    console.log(`Server Started at ${3000}`)
})

//Routes
const authRouter = require('./routes/auth');
//route for all authentication request
app.use('/auth', authRouter);
