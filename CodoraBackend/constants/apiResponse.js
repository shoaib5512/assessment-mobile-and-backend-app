module.exports = ()=> {
    return {
        success: true,
        message: "" ,
        data: {},
        error : { 
            code: null, 
            message: "" ,
        },
    };
} ;