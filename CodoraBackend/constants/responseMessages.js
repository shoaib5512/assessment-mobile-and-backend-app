module.exports = {
  usernameRequired: 'Username field is required.',
  emailRequired: 'Email field is required.',
  emailAlreadyExist: 'Email is already exist.',
  invalidemail: ' Please provide valid email address.',
  passwordRequired: 'Please enter strong password',
}