

# Assessment and Backend project



## 1 Getting started
- Node.js MongoDB Project
- This is a Node.js project using MongoDB as the database. It demonstrates how to connect to - MongoDB, perform CRUD operations, and run a Node.js server.

## 2 Prerequisites
Before running this project, you need to have the following installed on your machine:

# Node.js: Download and install Node.js
# MongoDB: Download and install MongoDB

i- navigate to project directory  and run "npm install"

# 3 Setting Up MongoDB Locally:
# (i) Install Mongo DB
# (ii) Start Mongo DB Locally
# (iii)  after installation check  and run command in terminal: 'mongod'


# 4- Start project
To start the Node.js server, run: npm start


