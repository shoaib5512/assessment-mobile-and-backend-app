const express = require('express');
const router = express.Router();
const User = require('../models/User');
const apiResponseTemplate = require('../constants/apiResponse');
const { emailRequired, invalidemail, usernameRequired, passwordRequired, emailAlreadyExist } = require('../constants/responseMessages');
const { isValidEmail } = require('../helpers/validation');


router.post('/signup', async (req, res) => {
    //signup Post request
    let apiResponse = apiResponseTemplate();
    try {
        // Check if email already exists
        const existingUser = await User.findOne({ email: req.body.email });
        if (existingUser) {
            apiResponse.success = false;
            apiResponse.error = { code: 400, message: emailAlreadyExist };
            return res.status(400).json(apiResponse);
        }

        const userData = new User({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
        })
        const { username, email, password } = req.body;

        if (!username) {
            apiResponse.success = false;
            apiResponse.error = { code: 400, message: usernameRequired };
            return res.status(400).json(apiResponse);
        }

        if (!email) {
            apiResponse.success = false;
            apiResponse.error = { code: 400, message: emailRequired };
            return res.status(400).json(apiResponse);
        }

        if (!isValidEmail(email)) {
            apiResponse.success = false;
            apiResponse.error = { code: 400, message: invalidemail };
            return res.status(400).json(apiResponse);
        }

        if (!password) {
            apiResponse.success = false;
            apiResponse.error = { code: 400, message: passwordRequired };
            return res.status(400).json(apiResponse);
        }
        //success
        apiResponse.message = "Congratulations: Your account has been created successfully."
        await userData.save();
        res.status(200).json(apiResponse);


    }
    catch (error) {
        apiResponse.success = false;
        apiResponse.error.code = 500;
        apiResponse.error.message = error?.message || error?.message;
        res.status(500).json(apiResponse);
    }

});
module.exports = router;
